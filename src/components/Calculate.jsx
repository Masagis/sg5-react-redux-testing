import { useState } from "react"
import { Button, Col, Form, FormGroup, Input, Row } from "reactstrap";
import { connect } from "react-redux"

const Calculate = (props) => {
  const initialValues = {
    x: 0,
    y: 0,
    operator: "tambah",
  }

  const [values, setValues] = useState(initialValues)

  const handleChange = (e) => {
    const { name, value } = e.target
    setValues({
      ...values,
      [name]: value
    })
  }

  return (
    <div className="row mt-5">
      <div className="col-12">
        <h1>{props.title}</h1>
        <h2 className="text-center">{props.total}</h2>
        <Form>
          <Row form>
            <Col md={5}>
              <FormGroup>
                <Input name="x" type="number" placeholder="X value..." onChange={handleChange} />
              </FormGroup>
            </Col>
            <Col md={2}>
              <FormGroup>
                <Input type="select" name="operator" onChange={handleChange}>
                  <option value="tambah">+</option>
                  <option value="kurang">-</option>
                  <option value="kali">*</option>
                  <option value="bagi">/</option>
                </Input>
              </FormGroup>
            </Col>
            <Col md={5}>
              <FormGroup>
                <Input name="y" type="number" placeholder="Y value..." onChange={handleChange} />
              </FormGroup>
            </Col>
          </Row>
          <div className="text-center">
            <Button color="success" size="lg" onClick={() => props.calculateFunction(values)}>Calculate</Button>
          </div>
        </Form>
      </div>
    </div>
  )
}

const stateToProps = (globalState) => {
  return {
    total: globalState.total
  }
}

const dispatchToProps = (dispatch) => {
  return {
    calculateFunction: (values) => dispatch({ type: "CALCULATE", values: values })
  }
}

export default connect(stateToProps, dispatchToProps)(Calculate)
