import { useState } from 'react'

const Counter = () => {
    const [input, setInput] = useState(1)
    const [counter, setCounter] = useState(0)

    return (
        <>
            <h3 data-testid="title">Aplikasi Counter</h3>
            <h2 data-testid="counter">{counter}</h2>
            <button data-testid="btnPlus" onClick={() => setCounter(counter + input)}>+</button>
            <input data-testid="input" type="number" value={input} onChange={(e) => setInput(parseInt(e.target.value))}/>
            <button data-testid="btnMinus" onClick={() => setCounter(counter - input)}>-</button>
            <br/>
            <button data-testid="btnReset" onClick={() => {
                setCounter(0)
                setInput(1)
            }}>Reset</button>
        </>
    )
}

export default Counter
