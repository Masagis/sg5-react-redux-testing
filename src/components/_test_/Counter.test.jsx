import Counter from '../Counter'
import { render, fireEvent } from '@testing-library/react'

let getByTestId
beforeEach(() => {
  const component = render(<Counter title="Counter Apps" />)
  getByTestId = component.getByTestId
})

test("Ada judulnya", () => {
    const titleEl = getByTestId("title")
    expect(titleEl.textContent).toBe("Aplikasi Counter")  
})

test("ada nilai counter", () => {
    const counterEl = getByTestId("counter")
    expect(counterEl.textContent).toBe("0")
})

test("ada nilai input", () => {
    const inputEl = getByTestId("input")
    expect(inputEl.value).toBe("1")
})

test("ada tombol plus", () => {
    const btnPlus = getByTestId("btnPlus")
    expect(btnPlus.textContent).toBe("+")
})

test("ada tombol minus", () => {
    const btnMinus = getByTestId("btnMinus")
    expect(btnMinus.textContent).toBe("-")
})

test("ada tombol reset", () => {
    const btnReset = getByTestId("btnReset")
    expect(btnReset.textContent).toBe("Reset")
})

test("pastikan input berubah", () => {
    const inputEl = getByTestId("input")
  
    fireEvent.change(inputEl, {
      target: { value: "5" }
    })
    expect(inputEl.value).toBe("5")
  
    fireEvent.change(inputEl, {
      target: { value: "15" }
    })
    expect(inputEl.value).toBe("15")
  
    fireEvent.change(inputEl, {
      target: { value: "-42" }
    })
    expect(inputEl.value).toBe("-42")
  })

test("Button plus di klik", () => {
  const btnPlus = getByTestId("btnPlus")
  const counterEl = getByTestId("counter")
  
  fireEvent.click(btnPlus)
  expect(counterEl.textContent).toBe("1")
  fireEvent.click(btnPlus)
  expect(counterEl.textContent).toBe("2")
  fireEvent.click(btnPlus)
  expect(counterEl.textContent).toBe("3")
})

  test("Button minus di klik", () => {
    const btnMinus = getByTestId("btnMinus")
    const counterEl = getByTestId("counter")
  
    fireEvent.click(btnMinus)
    expect(counterEl.textContent).toBe("-1")
    fireEvent.click(btnMinus)
    expect(counterEl.textContent).toBe("-2")
    fireEvent.click(btnMinus)
    expect(counterEl.textContent).toBe("-3")
  })

test("Button reset di klik", () => {
  const btnMinus = getByTestId("btnMinus")
  const btnPlus = getByTestId("btnPlus")
  const btnReset = getByTestId("btnReset")
  const counterEl = getByTestId("counter")
  const inputEl = getByTestId("input")
    
  fireEvent.click(btnPlus)
  fireEvent.click(btnPlus)
  fireEvent.click(btnPlus)
  expect(counterEl.textContent).toBe("3")
  
    fireEvent.click(btnMinus)
    expect(counterEl.textContent).toBe("2")
  
    fireEvent.click(btnPlus)
    fireEvent.click(btnPlus)
    expect(counterEl.textContent).toBe("4")
  
    fireEvent.click(btnReset)
    expect(counterEl.textContent).toBe("0")
    expect(inputEl.value).toBe("1")
  })
  
  test("Input diubah kemudian di plus dan minus, akhirnya di reset", () => {
    const btnMinus = getByTestId("btnMinus")
    const btnPlus = getByTestId("btnPlus")
    const btnReset = getByTestId("btnReset")
    const counterEl = getByTestId("counter")
    const inputEl = getByTestId("input")
  
    fireEvent.change(inputEl, {
      target: { value: "10" }
    })
    expect(inputEl.value).toBe("10")
  
    fireEvent.click(btnMinus)
    expect(counterEl.textContent).toBe("-10")
    fireEvent.click(btnMinus)
    expect(counterEl.textContent).toBe("-20")
  
    fireEvent.change(inputEl, {
      target: { value: "30" }
    })
    expect(inputEl.value).toBe("30")
  
    fireEvent.click(btnPlus)
    expect(counterEl.textContent).toBe("10")
    fireEvent.click(btnPlus)
    expect(counterEl.textContent).toBe("40")
  
    fireEvent.click(btnReset)
    expect(counterEl.textContent).toBe("0")
    expect(inputEl.value).toBe("1")
  })