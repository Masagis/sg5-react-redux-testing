import types from './action'

const globalState = {
    counter: "0",
    x: "",
    y: "",
    operator: "+"
}

const rootReducer = (state=globalState, action) => {
    switch (action.type) {
        case "PLUS_COUNTER":
            return{
                ...state,
                counter: state.counter + action.input
            }
        case "MINUS_COUNTER":
            return{
                ...state,
                counter: state.counter - action.input
            }
        case "RESET_COUNTER":
            return{
                ...state,
                counter: 0,
            }
        default:
            return state
    }
}

export default rootReducer