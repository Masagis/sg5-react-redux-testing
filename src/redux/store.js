import { createStore } from 'redux'
import rootReducer from './reducer'

const globalStore = createStore(rootReducer)

export default globalStore